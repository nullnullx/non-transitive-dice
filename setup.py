#!/usr/bin/env python

from distutils.core import setup

setup(name='colorama',
      version='0.4.4',
      description='Makes ANSI escape character sequences (for producing colored terminal text and cursor positioning) work under MS Windows.',
      author='Jonathan Hartley',
      author_email='tartley@tartley.com',
      url='https://pypi.org/project/colorama/',
      packages=['colorama'],
     )