#!/usr/bin/env python3

"""
Test to study non-transitive dice behavior
https://singingbanana.com/dice/article.htm
Results are printed for each pair of dice and their winning score.
"""

import random
import itertools
import colorama
from colorama import Fore, Style

__author__ = "Andrey Mischenko"
__credits__ = ["Andrey Mischenko", "Andrey Risukhin"]
__version__ = "1.0.1"
__date__ = "06/15/2021"
__email__ = "nullnullx@gmail.com"
__status__ = "Development"

# Define number of rolls
ROLLS = 500

class Dice:
    def __init__(self, color: str, name: str, faces: list):
        """Initialize Dice object with color, name and pips distribution"""

        # Dice color
        self.color = color
        # Dice name
        self.name = name
        # Pips distribution on each face
        self.faces = faces
        # Current face
        self.face = 0
        # Current average value
        self.average = 0.0
        # Cumulative total
        self._total = 0
        # Roll number
        self._roll = 0

    def roll(self) -> int:
        self.face = random.choice(self.faces)
        self._roll += 1
        self._total += self.face
        self.average = self._total / self._roll
        return self.face

    def __str__(self) -> str:
        return f"{self.color}{self.name}"


def print_ordered(dice: list) -> None:
    if dice[1] > dice[3]:
        print(f"{dice[0]}({dice[1]:3d})->{dice[2]}({dice[3]:3d})", end='')
    else:
        print(f"{dice[2]}({dice[3]:3d})->{dice[0]}({dice[1]:3d})", end='')


def main():
    # Configure all dice with color, name and pips
    dice = [
        Dice(Fore.RED + Style.BRIGHT, "A", [4, 4, 4, 4, 4, 9]),
        Dice(Fore.YELLOW + Style.BRIGHT, "B", [3, 3, 3, 3, 8, 8]),
        Dice(Fore.BLUE + Style.BRIGHT, "C", [2, 2, 2, 7, 7, 7]),
        Dice(Fore.MAGENTA + Style.BRIGHT, "D", [1, 1, 6, 6, 6, 6]),
        Dice(Fore.GREEN + Style.BRIGHT, "E", [0, 5, 5, 5, 5, 5]),
    ]

    colorama.init(autoreset=True)

    # Build all combinations for pair of dice with their score against each other
    dice_comb = []
    for dice_pair in itertools.combinations(dice, 2):
        dice_comb.append([
              dice_pair[0], 0,
              dice_pair[1], 0,
            ])

    # Rep
    for n in range(ROLLS):
        # Roll all dice
        for die in dice:
            die.roll()
        # Update score for all dice pairs
        for dice_pair in dice_comb:
            if dice_pair[0].face > dice_pair[2].face:
                dice_pair[1] += 1
            else:
                dice_pair[3] += 1
        # Print results after each 5th roll
        if not (n % 5):
            print(f"{n:3d}: ", end='')
            for a in dice_comb:
                print_ordered(a)
                print("  ", end='')
            print()


if __name__ == "__main__":
    main()
