#!/usr/bin/env python3

"""
Test to study non-transitive dice behavior
https://singingbanana.com/dice/article.htm
"""

import random
import colorama
from colorama import Fore, Style

__author__ = "Andrey Mischenko"
__credits__ = ["Andrey Mischenko", "Andrey Risukhin"]
__version__ = "1.0.1"
__date__ = "06/15/2021"
__email__ = "nullnullx@gmail.com"
__status__ = "Development"


class Dice:
    def __init__(self, color: str, name: str, faces: list):
        """Initialize Dice object with color, name and pips distribution"""

        # Dice color
        self.color = color
        # Dice name
        self.name = name
        # Pips distribution on each face
        self.faces = faces
        # Current face
        self.face = 0
        # Current average value
        self.average = 0.0
        # Cumulative total
        self._total = 0
        # Roll number
        self._roll = 0

    def roll(self) -> int:
        self.face = random.choice(self.faces)
        self._roll += 1
        self._total += self.face
        self.average = self._total / self._roll
        return self.face

    def __str__(self) -> str:
        return f"{self.color}{self.name}: {self.average:.3f}"


def print_ordered(dice: list) -> None:
    dice_sorted = sorted(dice, key=lambda die: die.average, reverse=True)
    for die in dice_sorted:
        print(f"{die}  ->  ", end='')
    print()


def main():
    dice = [
        Dice(Fore.RED + Style.BRIGHT, "A", [4, 4, 4, 4, 4, 9]),
        Dice(Fore.YELLOW + Style.BRIGHT, "B", [3, 3, 3, 3, 8, 8]),
        Dice(Fore.BLUE + Style.BRIGHT, "C", [2, 2, 2, 7, 7, 7]),
        Dice(Fore.MAGENTA, "D", [1, 1, 6, 6, 6, 6]),
        Dice(Fore.GREEN + Style.BRIGHT, "E", [0, 5, 5, 5, 5, 5]),
    ]

    colorama.init(autoreset=True)

    for n in range(1000):
        for die in dice:
            die.roll()
        if not (n % 10):
            print(f"{n:5d}:  ", end='')
            print_ordered(dice)


if __name__ == "__main__":
    main()
